package request

import "gitlab.com/golang12/service/response"

type Request interface {
	SetUrl(url string)
	SetMethod(method string)
	SetHeader(header string, value string)
	SetBody(body []byte)
	Do() (error, response.Response)
}

type GenRequest interface {
	New() Request
}
