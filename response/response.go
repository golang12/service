package response

type Response interface {
	GetCode() int
	GetBody() ([]byte,error)
}
