package parser

type Parser interface {
	Unmarshal(b []byte, v interface{}) error
	Marshal(m interface{}) ([]byte, error)
}
