package service

import (
	"gitlab.com/stractoring/tattooapp/pushserver.backend/handler"
	"gitlab.com/stractoring/tattooapp/pushserver.backend/parser"
	"gitlab.com/stractoring/tattooapp/pushserver.backend/request"
)

type Service struct {
	Gen     request.GenRequest
	Parser  parser.Parser
	Handler handler.Handler
}